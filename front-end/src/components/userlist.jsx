import React, {useState, useEffect} from 'react';
import axios from 'axios'
import {useNavigate, useParams} from "react-router-dom"
import {Link} from "react-router-dom"

const UserList = () => {

  const {id} = useParams()
  const [users, setUser] = useState([])
  const navigate = useNavigate()
  useEffect(()=> {
    getUsers();
  },[])
  const getUsers = async() => {
    const response = await axios.get('http://localhost:4000/api/v1/players')
    setUser(response.data.data)
  }
  
  const deleteUsers = async(id) => {
    try {
      await axios.delete(`http://localhost:4000/api/v1/players/${id}`)
      getUsers()
      console.log(`delete has been executed`)
    } catch (error) {
      console.log(error)
    } 
  }
  
  const handleAdd = async() => {
    navigate('/add')
  }



  return (
    <div className='main'>
        <table className="table">
    <thead>
    <tr>
      <th scope="col">Id</th>
      <th scope="col">Email</th>
      <th scope="col">UserName</th>
      <th scope="col">Password</th>
      <th scope="col">Experience</th>
      <th scope='col'>lvl</th>
      <th scope='col'>Edit/Delete</th>
    </tr>
    </thead>
    <tbody>
    {users.map((user, idx) => (
      <tr key={idx}>
        <td>{user.id}</td>
        <td>{user.email}</td>
        <td>{user.username}</td>
        <td>..</td>
        <td>{user.experience}</td>
        <td>{user.lvl}</td>
        <td>
          <Link to={`edit/${user.id}`} className="btn btn-danger" >Update</Link>
          <button onClick={()=> deleteUsers(user.id)} className="btn btn-warning" >Delete</button>
        </td>
      </tr>
    ))}
    </tbody>
  </table>
  <button type="button" className="btn btn-primary" onClick={handleAdd}>Add</button>
  </div>    
  )
}

export default UserList